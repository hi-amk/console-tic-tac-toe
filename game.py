# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board
def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0

    for entry in board:

        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "

        output = output + str(entry)

        if n % 3 == 2:
            output = output + " |\n"
            output = output + line

        n = n + 1

    print(output)
    print()

current_board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for turn in range(9):
  # print current board
  print_board(current_board)

  # prompt the player, which number they would like to move to
  response = input("Where would " + current_player + " like to move? ")

  # update the current_board with the player's symbol
  current_board[int(response) - 1] = current_player

  # determining whether there is a winner
  if ((current_board[0] == "X" and current_board[1] == "X" and current_board[2] == "X") or
  (current_board[3] == "X" and current_board[4] == "X" and current_board[5] == "X") or
  (current_board[6] == "X" and current_board[7] == "X" and current_board[8] == "X") or
  (current_board[0] == "X" and current_board[3] == "X" and current_board[6] == "X") or
  (current_board[1] == "X" and current_board[4] == "X" and current_board[7] == "X") or
  (current_board[2] == "X" and current_board[5] == "X" and current_board[8] == "X") or
  (current_board[0] == "X" and current_board[4] == "X" and current_board[8] == "X") or
  (current_board[2] == "X" and current_board[4] == "X" and current_board[6] == "X")):
    print_board(current_board)
    print("X has won")
    exit()
  elif ((current_board[0] == "O" and current_board[1] == "O" and current_board[2] == "O") or
  (current_board[3] == "O" and current_board[4] == "O" and current_board[5] == "O") or
  (current_board[6] == "O" and current_board[7] == "O" and current_board[8] == "O") or
  (current_board[0] == "O" and current_board[3] == "O" and current_board[6] == "O") or
  (current_board[1] == "O" and current_board[4] == "O" and current_board[7] == "O") or
  (current_board[2] == "O" and current_board[5] == "O" and current_board[8] == "O") or
  (current_board[0] == "O" and current_board[4] == "O" and current_board[8] == "O") or
  (current_board[2] == "O" and current_board[4] == "O" and current_board[6] == "O")):
    print_board(current_board)
    print("O has won")
    exit()

  # alternate turns for player
  if (current_player == "X"):
    current_player = "O"
  else:
    current_player = "X"

print_board(current_board)
print("It's a tie!")